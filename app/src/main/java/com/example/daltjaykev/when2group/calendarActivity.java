package com.example.daltjaykev.when2group;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class CalendarActivity extends AppCompatActivity {
    private ServerRequestManager srm;
    private MyPageAdapter myPA;
    private int[][] scheduleArray;
    private Meeting meeting;
    private Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_calendar);

        srm = ServerRequestManager.getInstance();

        if (srm.isCreatingUserSchedule()) {
            scheduleArray = srm.getUserSchedule(srm.getUser().getId());
        } else {
            scheduleArray = User.createEmptySchedule();

            if (srm.getCreatingMeeting() == null) {
                // not creating meeting
                meeting = srm.getUser().getGroups().get(srm.getClickedGroupIndex()).getMeetings().get(srm.getClickedMeetingIndex());
            } else {
                // creating meeting
                meeting = srm.getCreatingMeeting();
            }
        }

        final List<Fragment> ourFragments = getFragments();
        myPA = new MyPageAdapter(getSupportFragmentManager(), ourFragments);
        ViewPager pager = (ViewPager) findViewById(R.id.dayScroller);
        pager.setOffscreenPageLimit(6);
        pager.setAdapter(myPA);

        submitButton = (Button) findViewById(R.id.submitButton);
        final int[][] listToSend = new int[7][24];
        submitButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                int count = 0;
                for(Fragment frag: ourFragments){
                    BlankFragment casty = (BlankFragment) frag;
                    listToSend[count++] = casty.getFinalArray();
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_send:
                submitButton.callOnClick();
                if (srm.isCreatingUserSchedule()) {
                    srm.getUser().setSchedule(scheduleArray);
                    srm.updateUserSchedule(srm.getUser().getId(), scheduleArray);
                } else {
                    if (srm.getCreatingMeeting() == null) {
                        // not creating meeting
                        srm.getUser().getGroups().get(srm.getClickedGroupIndex()).getMeetings().set(srm.getClickedMeetingIndex(), meeting);
                    } else {
                        // creating meeting
                        srm.setCreatingMeeting(meeting);
                    }
                }
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.send, menu);
        return true;
    }

    private List<Fragment> getFragments(){
        List<Fragment> fList = new ArrayList<Fragment>();

        fList.add(BlankFragment.newInstance("Monday", scheduleArray[1]));
        fList.add(BlankFragment.newInstance("Tuesday", scheduleArray[2]));
        fList.add(BlankFragment.newInstance("Wednesday", scheduleArray[3]));
        fList.add(BlankFragment.newInstance("Thursday", scheduleArray[4]));
        fList.add(BlankFragment.newInstance("Friday", scheduleArray[5]));
        fList.add(BlankFragment.newInstance("Saturday", scheduleArray[6]));
        fList.add(BlankFragment.newInstance("Sunday", scheduleArray[0]));

        return fList;
    }
}
