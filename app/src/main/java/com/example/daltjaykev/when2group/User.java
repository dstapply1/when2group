package com.example.daltjaykev.when2group;

import android.animation.IntArrayEvaluator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class User {
    private int id;
    private String email;
    private String firstName;
    private String lastName;
    private int[][] schedule;
    private ArrayList<Group> groups;

    public User(int id, String email, String firstName, String lastName, String schedule) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;

        // parse schedule out of string
        this.schedule = parseSchedule(schedule);

        groups = new ArrayList<Group>();
    }

    public User() {
        id = 0;
        email = "";
        firstName = "";
        lastName = "";
        schedule = new int[7][24];
        groups = new ArrayList<Group>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(int[][] schedule) {
        this.schedule = schedule;
    }

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    public static String scheduleToString(int[][] schedule) {
        String strSched = "";

        // convert schedule to string
        for (int day = 0; day < 7; day++) {
            strSched += "[";
            for (int hour = 0; hour < 24; hour++) {
                strSched += schedule[day][hour] + ",";
            }
            strSched = strSched.substring(0, strSched.length()-1);
            strSched += "],";
        }

        strSched = strSched.substring(0, strSched.length()-1);

        return strSched;
    }

    public static int[][] parseSchedule(String stringSchedule) {
        int[][] schedule = createEmptySchedule();

        Matcher matcher = Pattern.compile("\\[([^\\]]+)").matcher(stringSchedule);

        List<String> days = new ArrayList<>();

        int pos = -1;
        while (matcher.find(pos+1)){
            pos = matcher.start();
            days.add(matcher.group(1));
        }

        int dayCount = 0;
        for (String day: days) {
            String[] hours = day.split(",");
            int hourCount = 0;
            for (String hour: hours) {
                schedule[dayCount][hourCount++] = Integer.parseInt(hour.trim());
            }
            dayCount++;
        }

        return schedule;
    }

    public static int[][] createEmptySchedule() {
        int[][] schedule = new int[7][24];

        for (int day = 0; day < 7; day++) {
            for (int hour = 0; hour < 24; hour++) {
                schedule[day][hour] = 0;
            }
        }

        return schedule;
    }
}
