package com.example.daltjaykev.when2group;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class GroupActivity extends AppCompatActivity {
    private ServerRequestManager srm;
    private Group group;
    private ArrayList<Button> meetingButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_group);

        srm = ServerRequestManager.getInstance();

        group = new Group();

        if (!srm.isAuthenticated()) {
            NavUtils.navigateUpFromSameTask(this);
        } else {
            fillFields();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!srm.isAuthenticated()) {
            NavUtils.navigateUpFromSameTask(this);
        } else {
            fillFields();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // Save user info
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_add_meeting:
                srm.setCreatingMeeting(new Meeting());
                openNewMeetingActivity();
                return true;

            case R.id.action_edit_group:
                Toast.makeText(this, "Edit group not currently implemented.", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.current_group, menu);
        return true;
    }

    private void openNewMeetingActivity() {
        Intent i = new Intent(getApplicationContext(), NewMeetingActivity.class);
        startActivity(i);
    }

    private void fillFields() {
        group = srm.getUser().getGroups().get(srm.getClickedGroupIndex());
        TextView groupName = (TextView) findViewById(R.id.groupNameText);
        groupName.setText(group.getName());
        loadMeetings();
    }

    private void loadMeetings() {
        meetingButtons = new ArrayList<Button>();

        ArrayList<Meeting> meetings = srm.getGroupMeetings(group.getId());

        for (int i = 0; i < meetings.size(); i++) {
            // show groups
            Button meetingButton = new Button(this);
            meetingButton.setText(meetings.get(i).getTitle());
            meetingButton.setTextSize(24.0f);
            meetingButton.setId(100 + meetingButtons.size());

            // make button send to group screen
            meetingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openMeetingActivity(view.getId()-100);
                }
            });

            meetingButtons.add(meetingButton);
        }

        LinearLayout meetingList = (LinearLayout) findViewById(R.id.meetingList);
        meetingList.removeAllViews();
        for (Button meetingButton: meetingButtons) {
            meetingList.addView(meetingButton);
        }

        srm.getUser().getGroups().get(srm.getClickedGroupIndex()).setMeetings(meetings);
    }

    private void openMeetingActivity(int meetingIndex) {
        srm.setClickedMeetingIndex(meetingIndex);
        Intent i = new Intent(getApplicationContext(), MeetingActivity.class);
        startActivity(i);
    }
}
