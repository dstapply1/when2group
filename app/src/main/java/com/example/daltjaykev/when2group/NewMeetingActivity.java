package com.example.daltjaykev.when2group;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

public class NewMeetingActivity extends AppCompatActivity {
    private ServerRequestManager srm;
    private Meeting meeting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_new_meeting);

        srm = ServerRequestManager.getInstance();

        Button locationButton = (Button) findViewById(R.id.locationButton);
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMapActivity();
            }
        });

        Button scheduleButton = (Button) findViewById(R.id.scheduleButton);
        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openScheduleActivity();
            }
        });

        fillFields();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // Save user info
                srm.setCreatingMeeting(new Meeting());
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_send:
                saveNewMeeting();
                srm.setCreatingMeeting(new Meeting());
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.send, menu);
        return true;
    }

    private void openMapActivity() {
        meeting.setTitle(((EditText) findViewById(R.id.titleText)).getText().toString());
        meeting.setAgenda(((EditText) findViewById(R.id.agendaText)).getText().toString());
        srm.setCreatingMeeting(meeting);
        Intent i = new Intent(getApplicationContext(), MapsActivity.class);
        startActivity(i);
    }

    private void openScheduleActivity() {
        meeting.setTitle(((EditText) findViewById(R.id.titleText)).getText().toString());
        meeting.setAgenda(((EditText) findViewById(R.id.agendaText)).getText().toString());
        srm.setCreatingMeeting(meeting);
        Intent i = new Intent(getApplicationContext(), CalendarActivity.class);
        startActivity(i);
    }

    private void saveNewMeeting() {
        int groupId = srm.getUser().getGroups().get(srm.getClickedGroupIndex()).getId();
        String title = ((EditText) findViewById(R.id.titleText)).getText().toString();
        int[][] schedule = srm.getCreatingMeeting().getSchedule();
        String latitude = srm.getCreatingMeeting().getLatitude();
        String longitude = srm.getCreatingMeeting().getLongitude();
        String agenda = ((EditText) findViewById(R.id.agendaText)).getText().toString();
        int creator = srm.getUser().getId();

        srm.createMeeting(groupId, title, agenda, creator, schedule, latitude, longitude);
    }

    private void fillFields() {
        meeting = srm.getCreatingMeeting();

        ((EditText) findViewById(R.id.titleText)).setText(meeting.getTitle());
        ((EditText) findViewById(R.id.agendaText)).setText(meeting.getAgenda());
    }
}
