package com.example.daltjaykev.when2group;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ServerRequestManager srm;
    private ArrayList<Button> groupButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        srm = ServerRequestManager.getInstance();

        if (!isAuthenticated()) {
            openLoginActivity();
        } else {
            loadGroups();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        System.out.println("MAIN_ACTIVITY: RESUMED");

        if (!isAuthenticated())
            openLoginActivity();
        else if (srm.isRegInstance()) {
            srm.setRegInstance(false);
            openProfileActivity();
        } else {
            loadGroups();

            srm.setCreatingUserSchedule(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.groups, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                openProfileActivity();
                return true;

            case R.id.action_add_group:
                openCreateGroupActivity();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * returns false on authentication failed
     */
    private boolean checkSavedCreds() {
        SharedPreferences sharedPref = getSharedPreferences("W2GSavedCredentialsFile", 0);
        String email = sharedPref.getString(getString(R.string.saved_email), "null");
        String password = sharedPref.getString(getString(R.string.saved_password), "null");

        // authenticate with them
        if (!email.equals("null") && !password.equals("null")) {
            if (srm.authenticate(email, password) > 0) {
                // auth successful
                return true;
            } else {
                System.out.println("Saved Credentials Failed...");
            }
        } else {
            System.out.println("No Saved Credentials.");
        }

        return false;
    }

    private boolean isAuthenticated() {
        if (srm.isAuthenticated()) {
            return true;
        } else {
            if (checkSavedCreds()) {
                return true;
            }
        }

        return false;
    }

    private void openLoginActivity() {
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
    }

    private void openProfileActivity() {
        Intent i = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(i);
    }

    private void openCreateGroupActivity() {
        Intent i = new Intent(getApplicationContext(), CreateGroupActivity.class);
        startActivity(i);
    }

    private void openGroupActivity(int groupIndex) {
        srm.setClickedGroupIndex(groupIndex);
        Intent i = new Intent(getApplicationContext(), GroupActivity.class);
        startActivity(i);
    }

    private void loadGroups() {
        groupButtons = new ArrayList<Button>();

        ArrayList<Group> groups = srm.getUserGroups(srm.getUser().getId());

        for (int i = 0; i < groups.size(); i++) {
            // get group info
            Group groupInfo = srm.getGroup(groups.get(i).getId());
            if (groupInfo != null) {
                groups.get(i).setName(groupInfo.getName());
                groups.get(i).setSchedule(groupInfo.getSchedule());

                // show groups
                Button groupButton = new Button(this);
                groupButton.setText(groupInfo.getName());
                groupButton.setTextSize(24.0f);
                groupButton.setId(100 + groupButtons.size());

                // make button send to group screen
                groupButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openGroupActivity(view.getId()-100);
                    }
                });

                groupButtons.add(groupButton);
            } else {
                Log.e("MainActivity", "groupInfo null");
            }
        }

        LinearLayout groupList = (LinearLayout) findViewById(R.id.groupList);
        groupList.removeAllViews();
        for (Button groupButton: groupButtons) {
            groupList.addView(groupButton);
        }

        srm.getUser().setGroups(groups);
    }
}
