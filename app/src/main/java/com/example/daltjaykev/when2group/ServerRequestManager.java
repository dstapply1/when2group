package com.example.daltjaykev.when2group;

import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ServerRequestManager {
    private static ServerRequestManager instance = null;
    private final String serverUrl = "http://130.215.250.140:3000";
    private ServerRequest serverRequest;
    // App user information
    private User user;
    // used to force user to profile page on register
    private boolean isRegInstance;
    // Saved state info
    private int clickedGroupIndex;
    private int clickedMeetingIndex;
    private int[][] currentSchedule;
    private LatLng currentCoords;
    private Meeting creatingMeeting;
    private boolean creatingUserSchedule;

    private ServerRequestManager() {
        user = new User();
        isRegInstance = false;
        clickedGroupIndex = -1;
        clickedMeetingIndex = -1;
        currentSchedule = User.createEmptySchedule();
        currentCoords = new LatLng(0, 0);
        creatingUserSchedule = false;
    }

    public static ServerRequestManager getInstance() {
        if (instance == null)
            instance = new ServerRequestManager();

        return instance;
    }

    public User getUser() {
        return user;
    }

    public boolean isAuthenticated() {
        return user.getId() > 0;
    }

    /**
     * @param username
     * @param password
     * @return userId on success else 0
     */
    public int register(String username, String password) {
        serverRequest = new ServerRequest();
        logout();

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "register");
            requestBody.put("username", username);
            requestBody.put("password", password);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("registration result: " + result);
                    if (result.equals("success")) {
                        try {
                            user.setId(Integer.parseInt(json.getString("userId")));
                        } catch (NumberFormatException ne) {
                            System.err.println("Server returned invalid userId");
                            ne.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return user.getId();
    }

    /**
     * @param username
     * @param password
     * @return userId on success else 0
     */
    public int authenticate(String username, String password) {
        serverRequest = new ServerRequest();
        logout();

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "auth");
            requestBody.put("username", username);
            requestBody.put("password", password);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("authentication result: " + result);
                    if (result.equals("success")) {
                        try {
                            user.setId(Integer.parseInt(json.getString("userId")));
                        } catch (NumberFormatException ne) {
                            System.err.println("Server returned invalid userId");
                            ne.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return user.getId();
    }

    public void logout() {
        user = new User();
    }

    public User getUserInfo(int userId) {
        serverRequest = new ServerRequest();
        User retUser = null;

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "getUserInfo");
            requestBody.put("userId", userId);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("getUserInfo result: " + result);
                    if (result.equals("success")) {
                        try {
                            int returnedId = Integer.parseInt(json.getString("userId"));
                            if (returnedId == userId) {
                                String username = json.getString("username");
                                String fname = json.getString("firstName");
                                String lname = json.getString("lastName");
                                String schedule = json.getString("schedule");
                                retUser = new User(userId, username, fname, lname, schedule);
                            } else {
                                System.err.println("Wrong user information returned...");
                            }
                        } catch (NumberFormatException ne) {
                            System.err.println("Server returned invalid userId");
                            ne.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return retUser;
    }

    public int updateUserInfo(int userId, String firstName, String lastName, int[][] schedule) {
        serverRequest = new ServerRequest();
        int retId = 0;

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "updateUserInfo");
            requestBody.put("userId", userId);
            requestBody.put("firstName", firstName);
            requestBody.put("lastName", lastName);
            requestBody.put("schedule", User.scheduleToString(schedule));
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("updateUserInfo result: " + result);
                    if (result.equals("success")) {
                        try {
                            retId = Integer.parseInt(json.getString("userId"));
                        } catch (NumberFormatException ne) {
                            System.err.println("Server returned invalid userId");
                            ne.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return retId;
    }

    public int[][] getUserSchedule(int userId) {
        serverRequest = new ServerRequest();

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "getUserSchedule");
            requestBody.put("userId", userId);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("getUserSchedule result: " + result);
                    if (result.equals("success")) {
                        try {
                            int[][] schedule = User.parseSchedule(json.getString("schedule"));
                            return schedule;
                        } catch (NumberFormatException ne) {
                            System.err.println("Server returned invalid userId");
                            ne.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @param userId
     * @param schedule
     * @return userId on success, 0 on error
     */
    public int updateUserSchedule(int userId, int[][] schedule) {
        serverRequest = new ServerRequest();
        int retId = 0;

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "updateUserSchedule");
            requestBody.put("userId", userId);
            requestBody.put("schedule", User.scheduleToString(schedule));
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("updateUserSchedule result: " + result);
                    if (result.equals("success")) {
                        try {
                            retId = Integer.parseInt(json.getString("userId"));
                        } catch (NumberFormatException ne) {
                            System.err.println("Server returned invalid userId");
                            ne.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return retId;
    }

    public int createGroup(int userId, String name) {
        serverRequest = new ServerRequest();
        int groupId = 0;

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "createGroup");
            requestBody.put("creator", userId);
            requestBody.put("name", name);
            requestBody.put("schedule", User.scheduleToString(User.createEmptySchedule()));
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("createGroup result: " + result);
                    if (result.equals("success")) {
                        try {
                            groupId = Integer.parseInt(json.getString("groupId"));
                        } catch (NumberFormatException ne) {
                            System.err.println("Server returned invalid groupId");
                            ne.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return groupId;
    }

    public int createMeeting(int groupId, String title, String agenda, int creatorId, int[][] schedule, String latitude, String longitude) {
        serverRequest = new ServerRequest();
        int meetingId = 0;

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "createMeeting");
            requestBody.put("groupId", groupId);
            requestBody.put("title", title);
            requestBody.put("agenda", agenda);
            requestBody.put("creator", creatorId);
            requestBody.put("schedule", User.scheduleToString(schedule));
            requestBody.put("latitude", latitude);
            requestBody.put("longitude", longitude);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("createMeeting result: " + result);
                    if (result.equals("success")) {
                        try {
                            meetingId = Integer.parseInt(json.getString("meetingId"));
                        } catch (NumberFormatException ne) {
                            System.err.println("Server returned invalid groupId");
                            ne.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return meetingId;
    }

    public int addGroupMember(String email, int groupId, String status) {
        serverRequest = new ServerRequest();

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "addGroupMember");
            requestBody.put("email", email);
            requestBody.put("groupId", groupId);
            requestBody.put("status", status);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                //System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("addGroupMember result: " + result);
                    if (result.equals("success")) {
                        return 1;
                    } else {
                        // failed
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }

    public ArrayList<Group> getUserGroups(int userId) {
        serverRequest = new ServerRequest();
        ArrayList<Group> groups = new ArrayList<Group>();

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "getUserGroups");
            requestBody.put("userId", userId);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("getUserGroups result: " + result);
                    if (result.equals("success")) {
                        try {
                            JSONArray jsonGroups = new JSONArray(json.getString("groups"));

                            for (int i = 0; i < jsonGroups.length(); i++) {
                                JSONObject group = jsonGroups.getJSONObject(i);
                                groups.add(new Group(group.getInt("groupId"), "", group.getString("status")));
                            }

                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return groups;
    }

    public Group getGroup(int groupId) {
        serverRequest = new ServerRequest();
        Group group = null;

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "getGroup");
            requestBody.put("groupId", groupId);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("getUserGroups result: " + result);
                    if (result.equals("success")) {
                        group = new Group(groupId, json.getString("name"), User.parseSchedule(json.getString("schedule")));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return group;
    }

    public ArrayList<Meeting> getGroupMeetings(int groupId) {
        serverRequest = new ServerRequest();
        ArrayList<Meeting> meetings = new ArrayList<Meeting>();

        JSONObject requestBody = new JSONObject();
        try {
            requestBody.put("action", "getGroupMeetings");
            requestBody.put("groupId", groupId);
        } catch(Exception e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = serverRequest.getJSON(serverUrl + "/" + requestBody.getString("action"), requestBody.toString());
            if (json != null) {
                System.out.println(json.toString());
                try {
                    String result = json.getString("result");
                    System.out.println("getGroupMeetings result: " + result);
                    if (result.equals("success")) {
                        try {
                            JSONArray jsonMeetings = new JSONArray(json.getString("meetings"));

                            for (int i = 0; i < jsonMeetings.length(); i++) {
                                JSONObject meeting = jsonMeetings.getJSONObject(i);
                                meetings.add(new Meeting(meeting.getInt("id"), meeting.getString("title"), meeting.getString("agenda"), meeting.getInt("creator"), User.parseSchedule(meeting.getString("schedule")), meeting.getString("latitude"), meeting.getString("longitude")));
                            }

                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return meetings;
    }

    public void setRegInstance(boolean val) {
        isRegInstance = val;
    }

    public boolean isRegInstance() {
        return isRegInstance;
    }

    public User getUpdatedUser() {
        user = getUserInfo(user.getId());
        return user;
    }

    public int getClickedGroupIndex() {
        return clickedGroupIndex;
    }

    public void setClickedGroupIndex(int clickedGroupIndex) {
        this.clickedGroupIndex = clickedGroupIndex;
    }

    public int getClickedMeetingIndex() {
        return clickedMeetingIndex;
    }

    public void setClickedMeetingIndex(int clickedMeetingIndex) {
        this.clickedMeetingIndex = clickedMeetingIndex;
    }

    public LatLng getCurrentCoords() {
        return currentCoords;
    }

    public void setCurrentCoords(LatLng currentCoords) {
        this.currentCoords = currentCoords;
    }

    public int[][] getCurrentSchedule() {
        return currentSchedule;
    }

    public void setCurrentSchedule(int[][] currentSchedule) {
        this.currentSchedule = currentSchedule;
    }

    public Meeting getCreatingMeeting() {
        return creatingMeeting;
    }

    public void setCreatingMeeting(Meeting creatingMeeting) {
        this.creatingMeeting = creatingMeeting;
    }

    public boolean isCreatingUserSchedule() {
        return creatingUserSchedule;
    }

    public void setCreatingUserSchedule(boolean creatingUserSchedule) {
        this.creatingUserSchedule = creatingUserSchedule;
    }
}
