package com.example.daltjaykev.when2group;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class ServerRequest {

    public ServerRequest() {

    }

    public JSONObject getJSONFromUrl(String strURL, String requestBody) {
        StringBuilder result = new StringBuilder();
        JSONObject jObj = null;
        String json = "";
        HttpURLConnection urlConnection = null;

        try {
            URL url = new URL(strURL);
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");

            String input = requestBody;

            OutputStream os = urlConnection.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            System.out.println(urlConnection.getHeaderFields().toString());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

        }catch( Exception e) {
            e.printStackTrace();
        }
        finally {
            urlConnection.disconnect();
        }

        try {
            json = result.toString();
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return jObj;

    }

    public JSONObject getJSON(String url, String requestBody) {
        JSONObject jObj = null;
        Params param = new Params(url,requestBody);
        Request myTask = new Request();

        try{
            jObj= myTask.execute(param).get();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }catch (ExecutionException e){
            e.printStackTrace();
        }
        return jObj;
    }


    private static class Params {
        String url;
        String requestBody;

        Params(String url, String requestBody) {
            this.url = url;
            this.requestBody = requestBody;
        }
    }

    private class Request extends AsyncTask<Params, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(Params... args) {
            ServerRequest request = new ServerRequest();
            JSONObject json = request.getJSONFromUrl(args[0].url,args[0].requestBody);

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            super.onPostExecute(json);
        }
    }
}
