package com.example.daltjaykev.when2group;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {
    private ServerRequestManager srm;
    private EditText firstNameText, lastNameText;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_profile);

        srm = ServerRequestManager.getInstance();

        firstNameText = (EditText) findViewById(R.id.firstNameText);
        lastNameText = (EditText) findViewById(R.id.lastNameText);

        Button scheduleButton = (Button) findViewById(R.id.scheduleButton);
        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openScheduleActivity();
            }
        });

        if (!srm.isAuthenticated()) {
            NavUtils.navigateUpFromSameTask(this);
        } else {
            updateUserInfoFields();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateUserInfoFields();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // Save user info
                saveUserInfo();
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_logout:
                saveUserInfo();
                logout();
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile, menu);
        return true;
    }

    private void saveUserInfo() {
        String firstName = firstNameText.getText().toString();
        String lastName = lastNameText.getText().toString();
        srm.updateUserInfo(user.getId(), firstName, lastName, User.createEmptySchedule());
    }

    private void updateUserInfoFields() {
        user = srm.getUpdatedUser();
        if (user != null && user.getId() > 0) {
            firstNameText.setText(user.getFirstName());
            lastNameText.setText(user.getLastName());
        } else {
            System.out.println("UpdateUserInfoFields failed.");
        }
    }

    private void logout() {
        srm.logout();

        // clear saved creds
        SharedPreferences sharedPref = getSharedPreferences("W2GSavedCredentialsFile", 0);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.saved_email), "null");
        // TODO: encrypt password
        editor.putString(getString(R.string.saved_password), "null");
        editor.commit();
    }

    private void openScheduleActivity() {
        srm.setCreatingUserSchedule(true);
        Intent i = new Intent(getApplicationContext(), CalendarActivity.class);
        startActivity(i);
    }
}
