package com.example.daltjaykev.when2group;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ServerRequestManager srm;
    private Meeting meeting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        srm = ServerRequestManager.getInstance();
    }

    @Override
    protected void onStop() {
        super.onStop();

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_send:
                if (srm.getCreatingMeeting() == null) {
                    // not creating meeting
                    srm.getUser().getGroups().get(srm.getClickedGroupIndex()).getMeetings().set(srm.getClickedMeetingIndex(), meeting);
                } else {
                    // creating meeting
                    srm.setCreatingMeeting(meeting);
                }
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.send, menu);
        return true;
    }

    public void updateLocation(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
        Log.e("Map", "Location Updated");
    }

    public void updateLocation(LatLng loc) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 16));
        Log.e("Map", "Location Updated");
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            System.out.println("Need permissions");
        }

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        updateLocation(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));

        // click even handler
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                meeting.setLatitude(Double.toString(latLng.latitude));
                meeting.setLongitude(Double.toString(latLng.longitude));

                createPoint(latLng);
            }
        });

        if (srm.getCreatingMeeting() == null) {
            // not creating meeting
            meeting = srm.getUser().getGroups().get(srm.getClickedGroupIndex()).getMeetings().get(srm.getClickedMeetingIndex());
            createPoint(new LatLng(Double.parseDouble(meeting.getLatitude()), Double.parseDouble(meeting.getLongitude())));
        } else {
            // creating meeting
            meeting = srm.getCreatingMeeting();
            createPoint(new LatLng(Double.parseDouble(meeting.getLatitude()), Double.parseDouble(meeting.getLongitude())));
        }
    }

    private void createPoint(LatLng loc) {
        if (loc.latitude != 0 && loc.longitude != 0) {
            // create marker
            MarkerOptions markerOptions = new MarkerOptions();

            // setting marker position
            markerOptions.position(loc);

            // set title to display lat long
            markerOptions.title(loc.latitude + " : " + loc.longitude);

            // clears old marker
            mMap.clear();

            // move camera
            mMap.animateCamera(CameraUpdateFactory.newLatLng(loc));

            // place marker
            mMap.addMarker(markerOptions);
        }
    }
}
