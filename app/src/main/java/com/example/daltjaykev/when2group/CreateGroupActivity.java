package com.example.daltjaykev.when2group;

import android.graphics.Color;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class CreateGroupActivity extends AppCompatActivity implements AddMemberDialogFragment.AddMemberDialogListener {
    private ServerRequestManager srm;
    private Button addMemberButton;
    private ArrayList<Integer> memberRowIds;
    private ArrayList<Integer> statusRowIds;
    private ArrayList<String> memberEmails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_create_group);

        addMemberButton = (Button) findViewById(R.id.addMemberButton);
        addMemberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMember();
            }
        });

        memberRowIds = new ArrayList<Integer>();
        statusRowIds = new ArrayList<Integer>();
        memberEmails = new ArrayList<String>();

        srm = ServerRequestManager.getInstance();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_send:
                createGroup();
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.send, menu);
        return true;
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String memberEmail) {
        createMemberRow(memberEmail, "Pending");
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    private void createGroup() {
        if (srm.isAuthenticated()) {
            User user = srm.getUser();
            EditText groupName = (EditText) findViewById(R.id.groupNameText);
            int groupId = srm.createGroup(user.getId(), groupName.getText().toString());
            srm.addGroupMember(srm.getUpdatedUser().getEmail(), groupId, "Owner");
            for (String email : memberEmails) {
                srm.addGroupMember(email, groupId, "Pending");
            }
        } else {
            NavUtils.navigateUpFromSameTask(this);
        }
    }

    private void addMember() {
        DialogFragment newFragment = new AddMemberDialogFragment();
        newFragment.show(getSupportFragmentManager(), "addmember");
    }

    private void createMemberRow(String email, String status) {
        LinearLayout tableContent = (LinearLayout) findViewById(R.id.tableContent);
        LinearLayout memberRow = new LinearLayout(this);
        memberRow.setOrientation(LinearLayout.HORIZONTAL);
        memberRow.setBackgroundResource(R.drawable.border);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        memberRow.setLayoutParams(params);

        memberRow.setPadding(0, convertToPixels(5), 0, convertToPixels(5));

        // Member Email
        TextView member = new TextView(this);
        params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        member.setLayoutParams(params);

        member.setId(memberRowIds.size() + statusRowIds.size() + 200);
        memberRowIds.add(member.getId());

        member.setText(email);
        member.setTextColor(Color.BLACK);
        member.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        member.setTextSize(22.0f);

        // Member status
        TextView statusRow = new TextView(this);
        params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        statusRow.setLayoutParams(params);

        statusRow.setId(memberRowIds.size() + statusRowIds.size() + 200);
        statusRowIds.add(statusRow.getId());

        statusRow.setText(status);
        statusRow.setTextColor(Color.BLACK);
        statusRow.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        statusRow.setTextSize(22.0f);

        memberRow.addView(member);
        memberRow.addView(statusRow);
        tableContent.addView(memberRow);

        // keep track of emails
        memberEmails.add(email);
    }

    private int convertToPixels(int dp) {
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (dp*scale + 0.5f);

        return dpAsPixels;
    }
}
