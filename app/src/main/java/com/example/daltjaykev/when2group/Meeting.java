package com.example.daltjaykev.when2group;

public class Meeting {
    private int id;
    private String title;
    private String agenda;
    private int creatorId;
    private int[][] schedule;
    private String latitude;
    private String longitude;

    public Meeting(int id, String title, String agenda, int creatorId, int[][] schedule, String latitude, String longitude) {
        this.id = id;
        this.title = title;
        this.agenda = agenda;
        this.creatorId = creatorId;
        this.schedule = schedule;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Meeting() {
        this.id = 0;
        this.title = "";
        this.agenda = "";
        this.creatorId = 0;
        this.schedule = User.createEmptySchedule();
        this.latitude = "0";
        this.longitude = "0";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(int creatorId) {
        this.creatorId = creatorId;
    }

    public int[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(int[][] schedule) {
        this.schedule = schedule;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
