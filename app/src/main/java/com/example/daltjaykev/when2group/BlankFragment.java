package com.example.daltjaykev.when2group;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BlankFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BlankFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlankFragment extends Fragment {

    private String mParam1;
    public static boolean clickable = true;
    public static int[] ourSched;
    private OnFragmentInteractionListener mListener;

    public BlankFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlankFragment newInstance(String param1, int[] schedule) {
        BlankFragment fragment = new BlankFragment();
        Bundle args = new Bundle();
        if(param1.equals("Group Schedule")){
            clickable = false;
        }
        ourSched = schedule;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myView = inflater.inflate(R.layout.fragment_blank, container, false);

        handleSchedule(myView);

        // Change day of the week
        TextView doW = (TextView) myView.findViewById(R.id.dayOfTheWeekText);
        doW.setText(mParam1);

        if(clickable){
            final Button buttSevenAM = (Button) myView.findViewById(R.id.sevenAM);
            final Button buttEightAM = (Button) myView.findViewById(R.id.eightAM);
            final Button buttNineAM = (Button) myView.findViewById(R.id.nineAM);
            final Button buttTenAM = (Button) myView.findViewById(R.id.tenAM);
            final Button buttElevenAM = (Button) myView.findViewById(R.id.elevenAM);
            final Button buttTwelvePM = (Button) myView.findViewById(R.id.twelvePM);
            final Button buttOnePM = (Button) myView.findViewById(R.id.onePM);
            final Button buttTwoPM = (Button) myView.findViewById(R.id.twoPM);
            final Button buttThreePM = (Button) myView.findViewById(R.id.threePM);
            final Button buttFourPM = (Button) myView.findViewById(R.id.fourPM);
            final Button buttFivePM = (Button) myView.findViewById(R.id.fivePM);
            final Button buttSixPM = (Button) myView.findViewById(R.id.sixPM);

            buttSevenAM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[7] == 0){
                        ourSched[7] = 3;
                        buttSevenAM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[7] == 3){
                        ourSched[7] = 0;
                        buttSevenAM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttEightAM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[8] == 0){
                        ourSched[8] = 3;
                        buttEightAM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[8] == 3){
                        ourSched[8] = 0;
                        buttEightAM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttNineAM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[9] == 0){
                        ourSched[9] = 3;
                        buttNineAM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[9] == 3){
                        ourSched[9] = 0;
                        buttNineAM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttTenAM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[10] == 0){
                        ourSched[10] = 3;
                        buttTenAM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[10] == 3){
                        ourSched[10] = 0;
                        buttTenAM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttElevenAM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[11] == 0){
                        ourSched[11] = 3;
                        buttElevenAM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[11] == 3){
                        ourSched[11] = 0;
                        buttElevenAM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttTwelvePM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[12] == 0){
                        ourSched[12] = 3;
                        buttTwelvePM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[12] == 3){
                        ourSched[12] = 0;
                        buttTwelvePM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttOnePM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[13] == 0){
                        ourSched[13] = 3;
                        buttOnePM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[13] == 3){
                        ourSched[13] = 0;
                        buttOnePM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttTwoPM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[14] == 0){
                        ourSched[14] = 3;
                        buttTwoPM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[14] == 3){
                        ourSched[14] = 0;
                        buttTwoPM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttThreePM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[15] == 0){
                        ourSched[15] = 3;
                        buttThreePM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[15] == 3){
                        ourSched[15] = 0;
                        buttThreePM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttFourPM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[16] == 0){
                        ourSched[16] = 3;
                        buttFourPM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[16] == 3){
                        ourSched[16] = 0;
                        buttFourPM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttFivePM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[17] == 0){
                        ourSched[17] = 3;
                        buttFivePM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[17] == 3){
                        ourSched[17] = 0;
                        buttFivePM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
            buttSixPM.setOnClickListener(new View.OnClickListener(){
                public void onClick(View v){
                    if(ourSched[18] == 0){
                        ourSched[18] = 3;
                        buttSixPM.setBackgroundColor(Color.parseColor("#00ff00"));
                    }else if(ourSched[18] == 3){
                        ourSched[18] = 0;
                        buttSixPM.setBackgroundColor(Color.parseColor("#ff0000"));
                    }
                }
            });
        }

        return myView;
    }

    public void handleColors(Button myButt, int index){
        switch(ourSched[index]){
            // nobody, red
            case 0:
                myButt.setBackgroundColor(Color.parseColor("#ff0000"));
                break;
            // quarter, yellow
            case 1:
                myButt.setBackgroundColor(Color.parseColor("#FFFF00"));
                break;
            // half, lime-green
            case 2:
                myButt.setBackgroundColor(Color.parseColor("#6FDC6F"));
                break;
            // most, green
            case 3:
                myButt.setBackgroundColor(Color.parseColor("#00ff00"));
                break;
        }
    }

    public void handleSchedule(View myView){
        Button buttonSevenAM = (Button) myView.findViewById(R.id.sevenAM);
        Button buttonEightAM = (Button) myView.findViewById(R.id.eightAM);
        Button buttonNineAM = (Button) myView.findViewById(R.id.nineAM);
        Button buttonTenAM = (Button) myView.findViewById(R.id.tenAM);
        Button buttonElevenAM = (Button) myView.findViewById(R.id.elevenAM);
        Button buttonTwelvePM = (Button) myView.findViewById(R.id.twelvePM);
        Button buttonOnePM = (Button) myView.findViewById(R.id.onePM);
        Button buttonTwoPM = (Button) myView.findViewById(R.id.twoPM);
        Button buttonThreePM = (Button) myView.findViewById(R.id.threePM);
        Button buttonFourPM = (Button) myView.findViewById(R.id.fourPM);
        Button buttonFivePM = (Button) myView.findViewById(R.id.fivePM);
        Button buttonSixPM = (Button) myView.findViewById(R.id.sixPM);

        handleColors(buttonSevenAM, 7);
        handleColors(buttonEightAM, 8);
        handleColors(buttonNineAM, 9);
        handleColors(buttonTenAM, 10);
        handleColors(buttonElevenAM, 11);
        handleColors(buttonTwelvePM, 12);
        handleColors(buttonOnePM, 13);
        handleColors(buttonTwoPM, 14);
        handleColors(buttonThreePM, 15);
        handleColors(buttonFourPM, 16);
        handleColors(buttonFivePM, 17);
        handleColors(buttonSixPM, 18);
    }

    public int[] getFinalArray(){
        int[] finalSchedule = new int[ourSched.length];
        for(int k = 0; k < 24; k++){
            finalSchedule[k] = ourSched[k];
        }
        return finalSchedule;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
