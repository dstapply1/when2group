package com.example.daltjaykev.when2group;

import java.util.ArrayList;

public class Group {
    private int id;
    private String name;
    private int[][] schedule;
    private ArrayList<Meeting> meetings;
    private String status;

    public Group() {
        id = 0;
        name = "";
        schedule = User.createEmptySchedule();
        meetings = new ArrayList<Meeting>();
        status = "";
    }

    public Group(int id, String name, int[][] schedule) {
        this.id = id;
        this.name = name;
        this.schedule = schedule;

        meetings = new ArrayList<Meeting>();
        status = "";
    }

    public Group(int id, String name, String status) {
        this.id = id;
        this.name = name;
        this.schedule = User.createEmptySchedule();

        meetings = new ArrayList<Meeting>();
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(int[][] schedule) {
        this.schedule = schedule;
    }

    public ArrayList<Meeting> getMeetings() {
        return meetings;
    }

    public void setMeetings(ArrayList<Meeting> meetings) {
        this.meetings = meetings;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
