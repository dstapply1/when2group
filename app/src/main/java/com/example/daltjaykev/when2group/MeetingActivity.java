package com.example.daltjaykev.when2group;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

public class MeetingActivity extends AppCompatActivity {
    private ServerRequestManager srm;
    private Meeting meeting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_meeting);

        srm = ServerRequestManager.getInstance();

        if (!srm.isAuthenticated()) {
            NavUtils.navigateUpFromSameTask(this);
        } else {
            fillFields();
        }

        Button locationButton = (Button) findViewById(R.id.locationButton);
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMapActivity();
            }
        });

        Button scheduleButton = (Button) findViewById(R.id.scheduleButton);
        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openScheduleActivity();
            }
        });

        srm.setCreatingMeeting(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!srm.isAuthenticated()) {
            NavUtils.navigateUpFromSameTask(this);
        } else {
            fillFields();
        }

        srm.setCreatingMeeting(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                // Save user info
                NavUtils.navigateUpFromSameTask(this);
                return true;

            case R.id.action_edit_meeting:
                Toast.makeText(this, "Edit current_meeting not currently implemented.", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.current_meeting, menu);
        return true;
    }

    private void openMapActivity() {
        Intent i = new Intent(getApplicationContext(), MapsActivity.class);
        startActivity(i);
    }

    private void openScheduleActivity() {
        Intent i = new Intent(getApplicationContext(), CalendarActivity.class);
        startActivity(i);
    }

    private void fillFields() {
        meeting = srm.getUser().getGroups().get(srm.getClickedGroupIndex()).getMeetings().get(srm.getClickedMeetingIndex());

        ((TextView) findViewById(R.id.titleText)).setText(meeting.getTitle());
        ((TextView) findViewById(R.id.agendaText)).setText(meeting.getAgenda());
    }
}
